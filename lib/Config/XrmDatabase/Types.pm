package Config::XrmDatabase::Types;

# ABSTRACT: Types for Config::XrmDatabase;

use strict;
use warnings;

our $VERSION = '0.08';

use Type::Utils -all;
use Types::Standard qw( Enum CodeRef );
use Type::Library -base,
  -declare => qw( QueryReturnValue OnQueryFailure );

use namespace::clean;

declare QueryReturnValue,
  as Enum[ \1, 'value', 'reference', 'all' ];

declare OnQueryFailure,
  as Enum( [ \1, 'undef', 'throw']) | CodeRef;

# COPYRIGHT

1;
