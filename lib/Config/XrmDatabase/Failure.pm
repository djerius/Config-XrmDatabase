package Config::XrmDatabase::Failure;

# ABSTRACT: Exception class

use v5.26;
use warnings;

our $VERSION = '0.08';

use custom::failures::x::alias
  -suffix => '_failure',
  qw(
  key
  components
  file
  parameter
  internal
  query
  );

1;
